pmccabe (2.8-3) unstable; urgency=medium

  * Update Paul's new email.  Closes: #1013286.
  * Bump Standards-Version to 4.6.1.
  * Bump copyright year for debian/ work.

 -- Simon Josefsson <simon@josefsson.org>  Mon, 20 Jun 2022 23:41:10 +0200

pmccabe (2.8-2) unstable; urgency=medium

  * Apply upstream patches to self-test to make it fail when it should.
  * Apply upstream 'make clean' patch to fix --twice build.

 -- Simon Josefsson <simon@josefsson.org>  Sat, 23 Jan 2021 18:51:56 +0100

pmccabe (2.8-1) unstable; urgency=medium

  * Update URLs for new upstream location.
  * Update paths in autopkgtest self-tests.

 -- Simon Josefsson <simon@josefsson.org>  Mon, 18 Jan 2021 20:38:06 +0100

pmccabe (2.7b-2) unstable; urgency=medium

  [ Helmut Grohne <helmut@subdivi.de> ]
  * Fix FTCBFS: Let dpkg's buildtools.mk initialize CC. (Closes: #980187)

  [ Simon Josefsson ]
  * Don't specify build targets any more, fixed upstream.

 -- Simon Josefsson <simon@josefsson.org>  Sat, 16 Jan 2021 09:45:19 +0100

pmccabe (2.7b-1) unstable; urgency=low

  * New release from new upstream.
    - Fixes FTBFS on arch's with unsigned char.
    - Fixes typos, silencing lintian typo-in-manual-page.
  * Add autopkg testing using upstream regression test suite.
  * Update watch file.
  * Put NEWS and README file into /usr/share/doc/pmccabe/.

 -- Simon Josefsson <simon@josefsson.org>  Thu, 14 Jan 2021 14:12:08 +0100

pmccabe (2.7-3) unstable; urgency=low

  * Upload to unstable.

 -- Simon Josefsson <simon@josefsson.org>  Fri, 08 Jan 2021 19:50:46 +0100

pmccabe (2.7-2) experimental; urgency=low

  * Add new Homepage: URL.
  * Bump Standards-Version to 4.5.1.
  * Rewrite debian/rules to invoke dh.
  * Replace debian/compat (5) with debhelper-compat (13).  Closes: #965780.
  * Use debian/source/format 3.0 (quilt).
  * Add debian/upstream/metadata.
  * Add Rules-Requires-Root: no.
  * Fix some lintian warnings.
  * Add Vcs-Browser and Vcs-Git.
  * Use DEP5 debian/copyright.
  * Build with hardening.
  * Fix FTCBFS, patch from Helmut Grohne. Closes: #948131.
    + Specify targets to dh_auto_build as it defaults to including tests.

 -- Simon Josefsson <simon@josefsson.org>  Fri, 25 Dec 2020 17:51:11 +0100

pmccabe (2.7-1) experimental; urgency=low

  * Add myself as Uploader.
  * Add watch file.

 -- Simon Josefsson <simon@josefsson.org>  Fri, 25 Dec 2020 15:07:19 +0100

pmccabe (2.7) unstable; urgency=low

  * moved web site to http://people.debian.org/~bame/pmccabe
  * Fix bugs from Ahmad Jbara <ahmadjbara@gmail.com>
    1. no longer counts ^ as part of complexity like &&
    2. statement count now includes # of switch (but not case) statements
       which will break people's regression tests WARNING! WARNING!
  * Inspired by and/or code donated by Matt Hargett:
  * (LP: #499591) "add threshold capability" from Matt Hargett
  * (LP: #659550) -
  * added -x/-X options to select the old or Matt's "cpp" algorithm
  * WARNING WARNING WARNING!!!! in version 2.8, -X will become the
    default and which will likely break people's existing regression tests
  * used indent(1) to format the source -st -bap -bad -d0 -bli0 -nce
    -cli0 -ss -npcs -bs -di1 -nbc -psl -i4 -lp -ip0 -nfca -nfc1
  * made some things boolean and fixed a bunch of compiler warnings
  * added -fwhole-program speedup

 -- Paul Bame <bame@debian.org>  Fri, 20 May 2011 17:37:45 -0400

pmccabe (2.6) unstable; urgency=low

  * (LP: #364338) pdiff script change has bug (ubuntu)
  * (LP: #499589) infinite loop with namespaced struct variable declaration
  * Thanks to matt_hargett for bugs and patches
  * Fixed a problem parsing DOS-format CRLF files
  * Updated deb stds version and fixed some lintian warnings

 -- Paul Bame <bame@debian.org>  Fri, 06 Aug 2010 14:18:25 -0400

pmccabe (2.5) unstable; urgency=low

  * updated codechanges and pdiff to be bashism safe
  * updated test files
  * bump Build-Depends debhelper 3->5

 -- Paul Bame <bame@debian.org>  Wed, 30 Jul 2008 15:21:28 -0600

pmccabe (2.4+nmu2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix bashism in codechanges /bin/sh script (Closes: #486054)

 -- Chris Lamb <chris@chris-lamb.co.uk>  Tue, 09 Sep 2008 03:36:03 +0100

pmccabe (2.4+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix bashism in 'codechanges' script (Closes: #465351)
  * Bump Standards-Version to 3.7.3.

 -- Chris Lamb <chris@chris-lamb.co.uk>  Sat, 12 Apr 2008 04:59:33 +0100

pmccabe (2.4) unstable; urgency=low

  * add support for C++ namespaces

 -- Paul Bame <bame@riverrock.org>  Mon, 13 Aug 2007 21:14:22 -0600

pmccabe (2.3-1) unstable; urgency=low

  * you can use "class" as an identifier in C code now.  Thanks to
    xianjie zhang for an excellent bug report.

 -- Paul Bame <bame@debian.org>  Wed, 26 Nov 2003 12:06:55 -0700

pmccabe (2.2-3) unstable; urgency=low

  * tweek for running under buildd and whenever pmccabe not installed
    and . not in $PATH

 -- Paul Bame <bame@debian.org>  Wed, 12 Mar 2003 14:05:33 -0700

pmccabe (2.2-2) unstable; urgency=low

  * Tweek debian/copyright to follow policy

 -- Paul Bame <bame@debian.org>  Sun, 23 Feb 2003 13:03:18 -0700

pmccabe (2.2-1) unstable; urgency=low

  * Initial public release.
  * Closes: #177749

 -- Paul Bame <bame@debian.org>  Tue, 17 Dec 2002 10:07:41 -0700
